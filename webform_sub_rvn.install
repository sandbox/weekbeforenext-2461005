<?php

/**
 * @file
 *   Webform module install/schema hooks.
 */

/**
 * Implements hook_schema().
 */
function webform_sub_rvn_schema() {
  $schema = array();

  $schema['webform_sub_rvn_revisions'] = array(
    'description' => 'The base table for Webform submission revisions.',
    'fields' => array(
      'sid' => array(
        'description' => 'The unique identifier for this submission.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'svid' => array(
        'description' => 'The primary identifier for this version.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => 'The id of the user that completed this submission.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'submitted' => array(
        'description' => 'Timestamp of when the form was submitted.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'unique keys' => array(
      'sid' => array('sid'),
    ),
    'primary key' => array('svid', 'sid'),
  );

  $schema['webform_sub_rvn_submissions'] = array(
    'description' => 'Holds general information about submissions outside of field values.',
    'fields' => array(
      'sid' => array(
        'description' => 'The unique identifier for this submission.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'svid' => array(
        'description' => 'The primary identifier for this version.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'nid' => array(
        'description' => 'The node identifier of a webform.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'serial' => array(
        'description' => 'The serial number of this submission.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => 'The id of the user that completed this submission.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'is_draft' => array(
         'description' => 'Is this a draft of the submission?',
         'type' => 'int',
         'size' => 'tiny',
         'not null' => TRUE,
         'default' => 0,
      ),
      'submitted' => array(
        'description' => 'Timestamp of when the form was submitted.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'remote_addr' => array(
        'description' => 'The IP address of the user that submitted the form.',
        'type' => 'varchar',
        'length' => 128,
      ),
      'log' => array(
        'description' => 'The log entry explaining the changes in this version.',
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'default' => NULL,
      ),
    ),
    'primary key' => array('svid', 'sid'),
    'indexes' => array(
      'nid_uid_sid_svid' => array('nid', 'uid', 'sid', 'svid'),
      'nid_sid_svid' => array('nid', 'sid', 'svid'),
    ),
  );

  $schema['webform_sub_rvn_submitted_data'] = array(
    'description' => 'Stores all submitted field data for webform submissions.',
    'fields' => array(
      'svid' => array(
        'description' => 'The primary identifier for this version.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'nid' => array(
        'description' => 'The node identifier of a webform.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'sid' => array(
        'description' => 'The unique identifier for this submission.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'cid' => array(
        'description' => 'The identifier for this component within this node, starts at 0 for each node.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'no' => array(
        'description' => 'Usually this value is 0, but if a field has multiple values (such as a time or date), it may require multiple rows in the database.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '0',
      ),
      'data' => array(
        'description' => 'The submitted value of this field, may be serialized for some components.',
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('svid', 'nid', 'sid', 'cid', 'no'),
    'indexes' => array(
      'sid_nid_svid' => array('sid', 'nid', 'svid'),
      // For all but MS SQL Server databases, 64-character index is created on the data column after the schema is installed.
    ),
  );

  return $schema;
}


/**
 * Implements hook_install().
 */
function webform_sub_rvn_install() {

}

/**
 * Implements hook_uninstall().
 */
function webform_sub_rvn_uninstall() {

}